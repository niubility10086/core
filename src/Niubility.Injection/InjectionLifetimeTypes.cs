﻿namespace Niubility.Injection
{
    public enum InjectionLifetimeTypes
    {
        Transient = 0,
        Scoped = 1,
        Singleton = 2
    }
}