﻿namespace Niubility.Core
{
    public enum OrderByDirections
    {
        ASC = 0,
        DESC = 1
    }
}